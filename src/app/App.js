import React from 'react';
import ProductDetails from "./product/pages/ProductDetails";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import ProfileDetails from "./profile/pages/ProfileDetails";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/user-profile/:id" component={ProfileDetails} />
          <Route path="/product-details" component={ProductDetails} />
          <Route path="/" component={Home}/>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
import React from 'react';

const DetailsTwo = ({userName, gender, description}) => {
  return (<div>
    <div>
      <label>UserName: </label>
      <span>{userName}</span>
    </div>
    <div>
      <label>Gender: </label>
      <span>{gender}</span>
    </div>
    <div>
      <label>Description: </label>
      <span>{description}</span>
    </div>
  </div>)
};

export default DetailsTwo;
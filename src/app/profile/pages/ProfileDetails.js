import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getProfileDetails, getProfileLike} from "../actions/profileActions";
import DetailsTwo from "../components/DetailsTwo";

class ProfileDetails extends Component {
  componentDidMount() {
    this.props.getProfileDetails(this.props.match.params.id);
  }
  Handler(){
    const json= {
      userProfileId:this.props.match.params.id,
      likedBy:'anonymous user'
    }

    this.props.getProfileLike(json)
  }
  render() {
    const {userName, gender, description} = this.props.profileDetails;
    return (
      <div>
        <h3>User Profile</h3>
        <button onClick={this.Handler.bind(this)}>Like</button>
        <DetailsTwo userName={userName} gender={gender} description={description} />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  profileDetails: state.profile.profileDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getProfileDetails,getProfileLike
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileDetails);

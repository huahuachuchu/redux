export const getProfileDetails = (id) => (dispatch) => {
  fetch(`http://localhost:8080/api/user-profiles/${id}`)
    .then(response => response.json())
    .then(result => {
      dispatch({
        type: 'GET_PROFILE_DETAILS',
        profileDetails: result
      })
    })
};
export const getProfileLike = (data) => (dispatch) => {
  fetch('http://localhost:8080/api/like', {
      body: JSON.stringify(data),
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      }
    })
    .then(response => {
      dispatch({
        type: 'GET_PROFILE_LIKE'
      })
    });
};
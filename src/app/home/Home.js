import React from 'react';
import {Link} from "react-router-dom";

const Home = () => {
  return (
    <div>
      <h3>This is a beautiful home page.</h3>
      <ul>
        <li>
          <Link to='/product-details'>Go to Product Details Page</Link>
        </li>
      </ul>
      <h3>User Profile</h3>
      <ul>
        <li>
          <Link to='/user-profile/1'>User Profile1</Link>
        </li>
        <li>
          <Link to='/user-profile/2'>User Profile2</Link>
        </li>
      </ul>
    </div>
  )
};

export default Home;